Source: cif-tools
Section: science
Priority: optional
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders:
 Maarten L. Hekkelman <maarten@hekkelman.com>
Build-Depends:
 cmake,
 mrc,
 debhelper-compat (= 13),
 libboost-regex-dev,
 zlib1g-dev,
 libcifpp-dev (>= 7.0.7-2),
 libmcfp-dev
Standards-Version: 4.5.0
Homepage: https://github.com/PDB-REDO/cif-tools
Vcs-Browser: https://salsa.debian.org/med-team/cif-tools
Vcs-Git: https://salsa.debian.org/med-team/cif-tools.git
Rules-Requires-Root: no

Package: cif-tools
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 vim,
Description: Suite of tools to manipulate, validate and query mmCIF files
 This package contains a suite of tools for the manipulation of mmCIF files.
 .
 The structure of macro molecules is nowadays recorded in mmCIF files. Until
 recently however the ancient PDB file format was used by many programs but
 that format has since long been deprecated.
 .
 This package provides two tools, pdb2cif and cif2pdb, that can convert files
 from one format into the other, provided that data fits of course.
 .
 Other tools are cif-validate, cif-grep, cif-diff, cif-merge and mmCQL. The
 latter can be used to manipulate an mmCIF file as if it were a SQL like
 database using SELECT, UPDATE, INSERT and DELETE commands.
 .
 This package depends on libcifpp.
